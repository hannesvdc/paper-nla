picture = imread('dog.png');
pictureGray = rgb2gray(picture);
%imshow(pictureGray)
pictureGray = double(pictureGray);
compressed2 = zeros(size(pictureGray));
size(compressed2)
tic
Q = proto_fixed_rank(pictureGray, 200);
[U, sigma, V] = svd_direct(pictureGray, Q);
compressed = U*sigma*V';
elapsed = toc;
disp(['Randomized: ', num2str(elapsed), ' sec']);

tic
[U, S, V] = svd(pictureGray);
for i = 1:100
    compressed2 = compressed2 + U(:,i)*S(i,i)*V(:,i)';
end
elapsed = toc;
disp(['Dyadic decomposition: ', num2str(elapsed), ' sec']);


compressed = uint8(compressed);
compressed2 = uint8(compressed2);
figure
imshow(compressed)
title('Proto compression');
figure
imshow(compressed2)
title('SVD Polyadic compression');