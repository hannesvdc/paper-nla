%% Compare accuracy power iteration scheme and subspace iteration scheme
% Make matrix A with enough discarded info of singular values
N = 200;
sigma = 0:1:(N-1);
singular = (1/1.2).^sigma;
sigmamat = diag(singular);
U = orth(randn(N,N));
V = orth(randn(N,N));
A = U*sigmamat*V';
discarded_info = eps^(1/(2*2+1))*norm(A);

% Initialize loop
K = 5;
meansPower = zeros(N, 1);
meansSubspace = zeros(N,1);
% Loop over power q
for q = 0:3
    discarded_info = eps^(1/(2*q+1))*norm(A);
    lost_info = sum(svd(A) < discarded_info);
    disp(['q = ', num2str(q)]);
    disp('lost singular values ');
    disp(lost_info);
    disp('bound ');
    disp(discarded_info);
    
    % Loop over ranks l
    for l = 1:N
        %disp(['power: ', num2str(q), ', rank: ', num2str(l)])
        meanPower = 0;
        meanSubspace = 0;
        % Do the experiment K times
        for i=1:K
            QPower = power_fixed_rank(A, l, q);
            QSubspace = subspace_fixed_rank(A, l, q);
            errorPower = norm(A-QPower*QPower'*A)/norm(A);
            errorSubspace = norm(A-QSubspace*QSubspace'*A)/norm(A);
            meanPower = meanPower + errorPower;
            meanSubspace = meanSubspace + errorSubspace;
        end
        meansPower(l) = meanPower/K;
        meansSubspace(l) = meanSubspace/K;
    end
    figure(q+1)
    semilogy(meansPower)
    hold on
    semilogy(meansSubspace)
    title(['Comparison mean error power iteration and subspace', ...
            ' iteration for power ',num2str(q)])
    legend('mean error Power iteration', 'mean error Subspace iteration')
    xlabel('rank l')
    ylabel('relative error')
end
