%% Experiment section 74

% setup matrix A
n = 2048;
A = 10*randn(n,n);
timings = zeros(10, 3);
errors = zeros(10, 3);
tests = 10;

for i = 1:1:8
    disp(i)
    l = 10 * 2^(i-1);
    if l > n
        break;
    end
    
    % direct method
    elapsed = 0;
    for k = 1:tests
        tic;
        [Qd, ~] = qr_householder(A(:, 1:l));
        [U, sigma, V] = svd_direct(A, Qd);
        elapsed = elapsed + toc;
    end
    timings(i, 1) = elapsed/tests;
    errors(i, 1) = norm(A - U*sigma*V')/norm(A);
        
    % gauss
    elapsed = 0;
    for k = 1:tests
        tic;
        Q = power_fixed_rank(A, l, 3);
        [U, sigma, V] = svd_direct(A, Q);
        elapsed = elapsed + toc;
    end
    timings(i, 2) = elapsed/tests;
    errors(i, 2) = norm(A-U*sigma*V')/norm(A);
        
    % Matlab svd
    elapsed = 0;
    for k = 1:tests    
        tic;
        [U, sigma, V] = svd(A);
        elapsed = elapsed + toc;
    end
    timings(i, 3) = elapsed/tests;
end