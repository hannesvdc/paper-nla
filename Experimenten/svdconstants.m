% vector with timings and the sizes
svdtimings = [];
prototimings = [];
sizes = [];

% Define the starting matrix size and the number of tests to average out
n = 2;
tests = 10;

% Define the matrix A
N = 2^9;
sigma = linspace(1,N,N);
sigma = 1000* (1/1.005).^sigma;
disp(max(sigma));
disp(min(sigma));
A = orth(randn(N,N))*diag(sigma)*(orth(randn(N,N)))';
disp('setup A');

% and the precision
eps = 0.001;
r = 2;

% For each size of the matrix, run 'tests' tests and average the timings
while n <= N
    disp(n);
    sizes = [sizes, n];
    B = A(1:n,1:n);
    
    % Time the matlab svd implementation
    elapsed = 0.0;
    for k=1:tests
        tic;
        [U, S, V] = svd(B);
        elapsed = elapsed + toc;
    end
    svdtimings = [svdtimings, elapsed/tests];
    
    % Proto algorithm for stage A, svd direct via stage B
    for k=1:tests
        tic;
        Q = proto_fixed_precision(B, eps, r);
        [U1, S1, V1] = svd_direct(B, Q);
        elapsed = elapsed + toc;
    end
    prototimings = [prototimings, elapsed/tests];
    
    % Double the value of n for the size of the next matrix.
    n = 2*n;
end

% And produce a plot with these timings
loglog(sizes,svdtimings);
hold on;
loglog(sizes, prototimings);
legend('matlab svd', 'proto svd');