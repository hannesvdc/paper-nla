%% Comparison of the execution time of the SVD computation via the proto and power iteration. 

% Define the matrix on which we perform the tests. It has a rapidly 
% decaying spectrum
sigma = linspace(1,50,50);
sigma = 1000* (1/1.5).^sigma;
A = orth(randn(50,50))*diag(sigma)*(orth(randn(50,50)))';

% Define the initial precision and q and r
eps = 1;
q = 1;
r = 2;

% Define variables for the errors and timings.
sum1 = 0;
sum2 = 0;
err1 = 0.0;
err1avg = 0.0;
err2 = 0.0;
err2avg = 0.0;
tests = 1000;
timings1 = [];
timings2 = [];
errors1 = [];
errors2 = [];
precisions = [];

% Iterate over a couple of values for the precision. For each value we 
% obtain an execution time and maximal and average error.
while eps >= 1.e-4
    err1 = 0.0;
    err2 = 0.0;
    err1avg = 0.0;
    err2avg = 0.0;
    time1 = 0.0;
    time2 = 0.0;

    precisions = [precisions, eps];
    
    % To obtain averages, perform 1000 experiments.
    for k = 1:1:tests
            
        % Proto algorithm for stage A, svd direct via stage B
        tic;
        Q = proto_fixed_precision(A, eps, r);
        [U1, S1, V1] = svd_direct(A, Q);
        err1 = max(err1, norm(A-U1*S1*V1'));
        err1avg = err1avg + norm(A-U1*S1*V1');
        elapsed = toc;
        time1 = time1 + elapsed;
        
        %  Power algorithm for stage A, svd direct via stage B
        tic;
        Q = power_fixed_precision(A, eps, r, q);
        [U1, S1, V1] = svd_direct(A, Q);
        err2 = max(err1, norm(A-U1*S1*V1'));
        err2avg = err2avg + norm(A-U1*S1*V1');
        elapsed = toc;
        time2 = time2 + elapsed;
    end
    
    timings1 = [timings1, time1];
    timings2 = [timings2, time2];
    errors1 = [errors1, err1avg/tests];
    errors2 = [errors2, err2avg/tests];
    
    % And update the precision for the next set of experiments.
    eps = eps/10;
end

data = zeros(5, 4);
data(:, 1) = errors1; data(:, 2) = errors2; data(:, 3) = timings1; data(:, 4) = timings2;
