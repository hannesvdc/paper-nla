%% Experiment section 72

% Generate n x n pixel grayscale image as described. Resulting Hermitian 
% matrix A has slowly decaying eigenvalues.
n = 30;
W = zeros(n^2, n^2); 
image = imread('dog.jpg');
image = rgb2gray(image);
image = imresize(image, [n,n]);
image = double(image);
image = round(4095/255*image);
X = zeros(25,n^2);
for i = 1:n
    for j = 1:n
        try
            X(:,j+(i-1)*n) = reshape(image(i-2:i+2,j-2:j+2),[25,1]);
        catch
            X(:,j+(i-1)*n) = image(i,j).*linspace(0.9,1.1,25)';
        end
%         X(:,j+(i-1)*n) = image(i,j).*linspace(0.8,1.2,25);
    end
end
sigma = 50;
for i = 1:n^2
    for j = 1:n^2
        W(i,j) = exp(-norm(X(i)-X(j))^2/sigma^2);
    end
end

for i = 1:n^2
    x = W(i, :);
    sortedX = sort(x, 'descend');
    x(x < sortedX(7)) = 0;
    W(i, :) = x;
end

D = diag(sum(W,2));
A = D^(-1/2)*W*D^(-1/2);

% Compute power fixed rank for each power q and rank l
nb_it = 100;
nb_q = 4;
errors = zeros(nb_q,nb_it);
for l = 1:nb_it
    disp(l)
    for q = 1:nb_q
        Q = power_fixed_rank(A, l, q-1);
        errors(q,l) = norm(A-Q*Q'*A);
    end
end
% Plot eigenvalues
S = svd(A);
figure(1);
plot(S(2:nb_it+1),'-x')
hold on

% Plot errors for each power q
for q = 1:nb_q
    plot(errors(q, :),'-x')
    hold on
end

title('Approximation error el')
legend('exact','q = 0', 'q = 1', 'q = 2', 'q = 3')
xlabel('l')
ylabel('magnitude')

% Plot estimated eigenvalues
figure(2);
E = eig(A);
% plot(S(1:nb_it),'-x')
lambda = sort(abs(E),'descend');
plot(lambda(1:nb_it), '-x')
hold on
l = nb_it;
for q = 1:nb_q
    Q = power_fixed_rank(A, l, q-1);
    [~, lambda] = eig_direct(A,Q);
    lambda = sort(diag(abs(lambda)),'descend');
    plot(lambda(1:nb_it), '-x')
    hold on
end
title('Estimated Eigenvalues lambda')
legend('exact','q = 0', 'q = 1', 'q = 2', 'q = 3')
xlabel('l')
ylabel('magnitude')


