%% Experiment section 7.1

% Generate N x N matrix with rapidly decaying singular values
N = 200;
sigma = 1:1:N;
singular = (1/1.2).^sigma;
sigmamat = diag(singular);
U = orth(randn(N,N));
V = orth(randn(N,N));
A = U*sigmamat*V';

% Define tolerance
eps = 10^-14;
r = 5;
maxit = N;

% Compute orthonormal matrix Q as approximation of the range of A
[Q, errors] = proto_fixed_precision(A, eps, r, maxit);

% Define upper and lower bound
theory = singular(1:(N-1));
upper = zeros(N,1);
for k = 1:1:(N-1)
    upper(k) = (1+9*sqrt(k)*sqrt(200))*singular(k+1);
end

% Plot the numerical error, singular values and theoretical upperbound
figure(1);
semilogy(errors);
hold on
semilogy(singular);
hold on;
semilogy(upper);
title('Upper and lower bounds errors and numerical errors');
legend('numerical error', 'singular value', 'upper bound');
xlabel('Index');
ylabel('Approximation error');
