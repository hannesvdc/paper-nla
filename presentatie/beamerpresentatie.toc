\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Introduction}{7}{0}{2}
\beamer@sectionintoc {3}{Stage A: Computing low rank approximation}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Randomized Proto Algorithm}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Randomized Power Iteration}{19}{0}{3}
\beamer@sectionintoc {4}{Stage B: Applying low rank approximation}{22}{0}{4}
\beamer@subsectionintoc {4}{1}{Eigenvalue Decomposition}{23}{0}{4}
\beamer@subsectionintoc {4}{2}{Singular Value Decomposition}{26}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{31}{0}{5}
