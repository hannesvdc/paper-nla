\documentclass[kul]{kulakbeamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{framed}
\usepackage{listings} 
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}
\usepackage{algorithm}
\usepackage{algorithmic}

\captionsetup{font=scriptsize,labelfont=scriptsize}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\title[Finding Structure with Randomness]{Finding Structure with Randomness}
\subtitle{Probabilistic Algorithms for
	Constructing\\ Approximate
	Matrix Decompositions}
\author{Simon Geirnaert\\Hannes Vandecasteele}
\institute[Kulak]{KU Leuven}
\date{Academic year 2016 - 2017}

% Overzicht bij het begin van elk hoofdstuk 
\AtBeginSection[]{\only<beamer>{\addtocounter{framenumber}{-1}
	\begin{outlineframe}[Overview]
		\tableofcontents[currentsection]
	\end{outlineframe}}
	}
\begin{document}

\begin{titleframe}
\titlepage
\end{titleframe}

\begin{outlineframe}[Outline]
\tableofcontents
\end{outlineframe}

 % % % Here you go  % % % 

\section{Motivation}

\begin{frame}
\frametitle{Motivation: matrix decompositions}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth, height=0.3\textheight,keepaspectratio]{Header_influencing_algo.png}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth, height=0.9\textheight,keepaspectratio]{influencing_algo.png}
	\caption{Snippet from article \cite{cipra2000best}}
\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Motivation: low-rank approximations}
	\begin{block}{Low-rank approximation}
	\centering
	 $\underset{m \times n}{A} \approx \underset{m \times k}{B} \underset{k \times n}{C}$
	\end{block}
	\begin{figure}
		\centering
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[width=\linewidth]{pca.png}
		\end{subfigure}%
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[width=\linewidth]{../Experimenten/dog}
		\end{subfigure}
	\end{figure}
%	\begin{figure}
%		\centering
%		\includegraphics[width=\textwidth, height=0.3\textheight,keepaspectratio]{pca.png}
%	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Motivation: why randomize?}
	Randomization is a powerful tool, used in many applications in scientific computing, e.g. Monte Carlo integration, \dots
	
	In the context of matrix decompositions:
	\begin{itemize}
		\item Simple and effective
		\item Fast
		\item Flexible: trade accuracy for speed
	\end{itemize}
\end{frame}



\section{Introduction}

\begin{frame}
\frametitle{Low-rank approximation: two computational stages}
\begin{itemize}
	\item \textbf{Stage A:} Construct low-dimensional subspace 
	\begin{itemize}
		\item Use randomized techniques
		\item Capture most of the action of $A$
	\end{itemize}
	\vspace{3mm}
	\item \textbf{Stage B:} Restrict matrix to subspace and compute something interesting
	\begin{itemize}
		\item SVD
		\item \dots
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Stage A: Construct low-dimensional subspace}
	\emph{Goal:} find approximate basis for the range of matrix $A$. More concrete, find $Q$ for which holds: 
	\[
	Q \text{ has orthonormal columns and } A \approx QQ^*A
	\]
	Graphically:
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth, height=\textheight,keepaspectratio]{stage_A_graph}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Stage B: Compute certain factorization}
	\emph{Goal:} compute a certain factorization (SVD, EVD, \dots), given matrix $Q$.
	\begin{block}{Example: SVD: $A \approx USV^*$}
		\centering
		\begin{enumerate}
			\item Form $C = Q^*A$
			\item Compute SVD of small matrix: $C = \widetilde{U}SV^*$
			\item Set $U = Q\widetilde{U}$
		\end{enumerate}
	\end{block}
	
	Recall: 
	\[
		 \underset{m \times n}{A} \approx \underset{m \times k}{B} \underset{k \times n}{C}
	\]
\end{frame}

\begin{frame}
	\frametitle{Different problem formulations}
	Two possiblities to approximate:
	\begin{itemize}
		\item Fixed rank approximation, given rank $k$, find $Q \in \mathbb{R}^{m \times k}$ such that
		\[
		\norm{A-QQ^*A}_F \approx \underset{\text{rank}(\overline{A})\leq k}{\text{min}}\norm{A-\overline{A}}_F.
		\]
		\item Fixed precision approximation, given $\epsilon$, find $Q \in \mathbb{R}^{m \times k(\epsilon)}$ such that 
		\[
		\norm{A-QQ^*A}_F < \epsilon.
		\]
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{The idea of randomization}
	\emph{Goal:} Seek basis for range of $A$ with rank $k$. 
	
	\begin{block}{Idea of constructing basis}
		\begin{itemize}
			\item Pick set of random vectors $\{\omega^{(i)}\in \mathbb{R}^{n \times 1}: i = 1, \dots, k\}$.
			\item Take corresponding samples of the range of $A: y^{(i)} = A\omega^{(i)} \in \mathbb{R}^{m \times 1}$.
			\item Orthonormalize sample vectors.
		\end{itemize}
	\end{block}
\end{frame}





\section[Stage A]{Stage A: Computing low rank approximation}
\subsection{Randomized Proto Algorithm}
\begin{frame}

\frametitle{Proto Algorithm: Fixed Rank}

	\begin{algorithm}[H]
		\caption{Proto Algorithm Fixed Rank}
		\begin{algorithmic}
			\STATE Given matrix $A \in \mathbb{R}^{m \times n}$ and target rank $k$:\\
			\begin{enumerate}
				\item Draw random Gaussian $\Omega \in \mathbb{R}^{n \times k}$
				\item Form $A \Omega$
				\item Compute a QR factorization $QR = A \Omega$
			\end{enumerate}
		\end{algorithmic}
	\end{algorithm}

	Computes a fixed rank approximation of $A$ , but no idea about the precision $\norm{A-QQ^*A}_F$.
\end{frame}

\begin{frame}
	\frametitle{Theoretical Error Bounds}
	Best rank-$k$ approximation of a matrix $A$:
	\begin{block}{Eckart-Young-Mirsky Theorem}
		\centering
		$\underset{X \in \mathbb{R}^{m \times n}, rank(X) = k}{\text{minimize}}\norm{A-X}_F$
		
		$X_k$ is given by
		$\sum_{i=1}^{k} u_i \sigma_i v_i^T$ with error $\norm{A-X_k}_ F=\sigma_{k+1}$
	\end{block}
	Similar result holds for a randomized approximation
	\[
	\norm{A-QQ^*A}_F \leq [1+9\sqrt{k\min(m,n)}]\sigma_{k+1}
	\]
	with probability greater than $1-10^{-r}$.
\end{frame}

\begin{frame}
	\frametitle{Experiment 1: Error bounds}
	Consider a $200 \times 200$ matrix $A$ with exponentially decaying singular values.
	\begin{figure}
		\includegraphics[width=0.6\linewidth]{fig71}
	\end{figure}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Proto Algorithm: Fixed precision}
	\begin{algorithm}[H]
		\caption{Proto Algorithm Fixed Precision}
		\begin{algorithmic}
			\STATE Draw random $\omega^{(i)}$ and form $A\omega^{(i)}, i=1\dots r$\\
			\STATE Form $Q^{(0)}$ from $A \Omega$\\
			\STATE j = r \\
			\WHILE{$\norm{A-QQ^{*}A}_F > \epsilon$}
			\STATE j = j+1
			\STATE Draw $\omega^{(j)}$ and compute $y = A\omega^{(j)}$
			\STATE Set $q^{(j)} = (I - Q^{(j-1)}(Q^{(j-1)})^{*})y$
			\STATE $Q^{(j)} = [Q^{(j-1)}, q^{(j)}]$
			\ENDWHILE
			\STATE $Q = Q^{(j)}$
		\end{algorithmic}
	\end{algorithm}
	Add orthogonal columns until the threshold is reached.
\end{frame}

\begin{frame}
	\frametitle{Practical implementations}
	\begin{itemize}
		\item Error estimation of $\norm{A-QQ^*A}_F$ by reusing the computed vectors $y^{(j)} = A\omega^{(j)}$
		\vspace{3mm}
		\item Fixed rank: compute until rank $k+p$, $p$ = oversampling parameter
		\vspace{3mm}
		\item Power algorithm for matrices with slowly decaying singular values
	\end{itemize}
\end{frame}

\subsection{Randomized Power Iteration}
\begin{frame}
	\frametitle{Randomized Power Iteration}
	Problem: Proto Algorithm produces poor basis when $A$ has a flat singular spectrum. \\
	
	Solution: \emph{Randomized Power Iteration}
	\begin{algorithm}[H]
		\caption{Randomized Power Iteration}
		\begin{algorithmic}
			\STATE Given matrix $A \in \mathbb{R}^{m \times n}$, power $q$ and target rank $l$:\\
			\begin{enumerate}
				\item Draw random Gaussian $\Omega \in \mathbb{R}^{n \times k}$
				\item Form $(AA^*)^qA\Omega$
				\item Compute a QR-factorization $(AA^*)^qA\Omega = QR$
			\end{enumerate} 
		\end{algorithmic}
	\end{algorithm}
	Say, $B = (AA^*)^qA$, easy to proof that: $\text{range}(B) = \text{range}(A)$, B has the same singular vectors and
	\[
	\sigma_j(B) =  \sigma_j(A)^{2q+1}, j = 1, 2, \dots
	\]
\end{frame}

\begin{frame}
	\frametitle{Randomized Subspace Iteration}
	Problem: with Randomized Power Iteration: very sensitive to rounding errors in floating-point arithmetic. \\
	
	Solution: \emph{Randomized Subspace Iteration}
	\begin{algorithm}[H]
	\caption{Randomized Subspace Iteration}
	\begin{algorithmic}
		\STATE Given matrix $A \in \mathbb{R}^{m \times n}$, power $q$ and target rank $l$:\\

			\STATE Draw random Gaussian $\Omega \in \mathbb{R}^{n \times k}$\\
			\STATE Form $A\Omega$ and compute its QR-factorization $Q_0R_0$\\
				\FOR {$j = 1,2, \dots, q$}
				\STATE Form $A^*Q_{j-1}$ and compute its QR-factorization $\hat{Q_j}\hat{R_j}$
				\STATE Form $A\hat{Q_j}$ and compute its QR-factorization $Q_jR_j$
				\ENDFOR
			\STATE $Q = Q_q$
	\end{algorithmic}
	\end{algorithm}	
\end{frame}

\begin{frame}
	\frametitle{Comparison Power Iteration and Subspace Iteration}
	Information, associated with singular values smaller than $\mu^{1/(2q+1)}\norm{A}$, $\mu$ = machine-precision, is lost
	\begin{figure}
		\centering
		\begin{subfigure}{.5\textwidth}
			\includegraphics[width=\textwidth, height=0.35\textheight]{pp_500_0}
		\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
			\includegraphics[width=\textwidth, height=0.35\textheight]{pp_500_1}
		\end{subfigure}

		\begin{subfigure}{.5\textwidth}
			\includegraphics[width=\textwidth, height=0.35\textheight]{pp_500_2}
		\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
			\includegraphics[width=\textwidth, height=0.35\textheight]{pp_500_3}
		\end{subfigure}
	\end{figure}
\end{frame}



\section[Stage B]{Stage B: Applying low rank approximation}
\subsection{Eigenvalue Decomposition}
\begin{frame}
	\frametitle{Direct Eigenvalue Decomposition}
	\begin{algorithm}[H]
		\caption{Direct Eigenvalue Decomposition}
		\begin{algorithmic}
			\STATE Given Hermitian matrix $A \in \mathbb{R}^{m \times m}$ and basis $Q \in \mathbb{R}^{m \times k}$:\\
			\begin{enumerate}
				\item Form the small matrix $B = Q^*AQ$
				\item Compute an eigenvalue decomposition $B = V\Lambda V^*$
				\item Form the orthonormal matrix $U = QV$
			\end{enumerate}
		\end{algorithmic}
	\end{algorithm}	
	Then the approximate eigenvalue decomposition is:
	\[
		A \approx U\Lambda U^*
	\]
	
\end{frame}

\begin{frame}
	\frametitle{Experiment 3: Matrix arising in image processing}
	
	\emph{Goal:} Evaluate direct eigenvalue decomposition with power iteration method
	
	Draw Hermitian matrix from intensities of picture:
	\begin{figure}
		\centering
		\includegraphics[width=\textwidth, height=0.5\textheight,keepaspectratio]{dog}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Experiment 3: Matrix arising in image processing}
	Results:
	\begin{figure}
		\centering
		\begin{subfigure}{.5\textwidth}
			\includegraphics[width=\textwidth, height=\textheight,keepaspectratio]{slow_decay_30_error}
		\end{subfigure}%
		\begin{subfigure}{.5\textwidth}
			\includegraphics[width=\textwidth, height=\textheight,keepaspectratio]{slow_decay_30_eig}
		\end{subfigure}
	\end{figure}
	
	
\end{frame}

\subsection{Singular Value Decomposition}
\begin{frame}
	\frametitle{Application to the SVD}
	\emph{Goal:} compute SVD of a given matrix $A$.
	\begin{block}{Example: SVD: $A \approx USV^*$}
		\centering
		\begin{enumerate}
			\item Form $C = Q^*A$
			\item Compute SVD of small matrix: $C = \widetilde{U}SV^*$
			\item Set $U = Q\widetilde{U}$
		\end{enumerate}
	\end{block}
	
	Recall: 
	\[
	\underset{m \times n}{A} \approx \underset{m \times k}{B} \underset{k \times n}{C}
	\]
\end{frame}

\begin{frame}
	\frametitle{Timings SVD Combinations}
	In the paper, they performed timings for different ways of computing the SVD:
	\begin{itemize}
		\item `Direct': Reduced QR factorization of $A$ + randomized SVD
		\item `Gauss': Proto algorithm + randomized SVD
		\item Deterministic MATLAB SVD implementation
	\end{itemize}
	\vspace{5mm}
	Disclaimer: MATLAB will always be faster due to their highly optimized Fortran implementation
\end{frame}

\begin{frame}
\frametitle{Numerical experiments}
Timings for a 1024 $\times$ 1024 and 2048 $\times$ 2048 matrix with increasing rank $l$
\begin{table}
	\centering
	\begin{tabular}{|c ||c | c || c | c |}
		\hline
		\emph{l} & \emph{Direct} & \emph{Gauss} & \emph{Direct} & \emph{Gauss} \\
		\hline
		10  & 0.3758  &  0.0206 & 3.2631 & 0.1037 \\
		20  & 0.3567  &  0.0309 & 3.2415 & 0.1495 \\
		40  & 0.3779  &  0.0528 & 3.2275 & 0.2487 \\
		80  & 0.4265  &  0.0595 & 3.2742 & 0.4559 \\
		160 & 0.6498  &  0.0595 & 3.9038 & 0.4137 \\
		320 & 1.5496  &  0.1566 & 5.6370 & 0.5985 \\
		640 & 4.9461  &  0.3852 & 12.2330 & 1.1477 \\
		1280 & -      &    -    & 36.4890 & 2.7557\\
		\hline
		\emph{svd} & 0.3146 & & 3.0589 & \\
		\hline
	\end{tabular}
\end{table}

\end{frame}

\begin{frame}
	\frametitle{Numerical results: Discussion}

	\begin{itemize}
		\item Gauss algorithm is always faster than the `Direct' method
		\vspace{3mm}
		\item In the paper, the randomized algorithms were mostly faster than their SVD implementation
		\vspace{3mm}
		\item Main criticism: they did not check the accuracy $\norm{A - U \Sigma V^T}_F$ of the obtained SVD
		\vspace{3mm}
		\item Better experiment: First define the accuracy $\epsilon$ and then compare the Gauss algorithm with the deterministic SVD
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Own experiment}
	\begin{figure}
		\includegraphics[width=\linewidth]{svdtimings}
		\label{fig:svdprotoruntime}
	\end{figure}

	Interpretation:

	\begin{itemize}
		\item MATLAB is faster due to optimized Fortran
		\item Roughly the same proportionality constant for both methods
	\end{itemize}
\end{frame}

\section{Conclusions}
\begin{frame}
	\frametitle{Conclusions}
	\begin{itemize}
		\item Verified all the claims of the authors - except timings in MATLAB
		\vspace{3mm}
		\item Performed own experiments to make the trade-off between precision and speed
		\vspace{3mm}
		\item Power algorithm better for certain matrices but less stable
		\vspace{3mm}
		\item Primer: Image compression of another dog
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Image compression experiment}
	3000 $\times$ 2500 picture
	\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{../Experimenten/dog}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Image compression results}
	\begin{figure}
		\centering
		\begin{subfigure}{0.5\textwidth}
			\centering
		\includegraphics[width=\linewidth]{../Experimenten/dograndom}
		\end{subfigure}%
		\begin{subfigure}{0.5\textwidth}
			\centering
			\includegraphics[width=\linewidth]{../Experimenten/dogpolyadic}
		\end{subfigure}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{References}
	\bibliographystyle{plain}
	\bibliography{biblio}
\end{frame}



\end{document}