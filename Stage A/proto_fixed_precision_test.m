function Q = proto_fixed_precision_test(A, eps, r)
    [m, n] = size(A);
    omega = randn(n, r);
    Y = zeros(m, 10*n);
    Y(:, 1:r) = A*omega;        % m x r
    tol = eps/(10*sqrt(2/pi));
    j = 0;
    Q = zeros(m, 0);             % m x ...
    
    % calculate the initial errors.
    norms = sqrt(sum(Y(:,1:r).^2, 1))';
    while max(norms) > tol
        %index
        j = j+1;
        size(Q)
        y = (eye(m) - Q*Q')*Y(:, j);
        Y(:, j) = y;
        q = y/norm(y);
        Q = [Q, q];
        
       % Q'*Q
        omega = rand(n, 1);
        Y(:, j+r) = (eye(m)-Q*Q')*(A*omega);
        
        for i = (j+1):(j+r-1)
            Y(:, i) = Y(:, i) - q*(q' * Y(:,i));
        end
        
        norms = sqrt(sum(Y(:,(j+1):(j+r)).^2, 1))';
        pause(1);
        %index = mod(index, r) + 1;
    end
end