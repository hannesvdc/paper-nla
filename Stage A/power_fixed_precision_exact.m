function [Q, steps] = power_fixed_precision_exact(A, eps, r, q)
    [m, n] = size(A);
    Omega = randn(n, r);
    Y = A*Omega;
    for i = 1:q
        Y = A'*Y;
        Y = A*Y;
    end
    j = 0;
    Q = zeros(m, 0);
    index = 1;

    while norm(A-Q*Q'*A) > eps
        j = j+1;
        j
        y = (eye(m) - Q*Q')*Y(:, index);
        q = y/norm(y);
        Q = [Q, q];
       
        omega = rand(n, 1);
        Y(:, index) = A*omega;
        for i = 1:q
            Y(:, index) = A'*Y(:, index);
            Y(:, index) = A* Y(:, index);
        end
        Y(:, index) = (eye(m)-Q*Q')*Y(:, index);
        
        for i = 1:r
            if i == index
                continue
            end
            Y(:, i) = Y(:, i) - q*(q' * Y(:,i));
        end
        
        index = mod(index, r) + 1;
    end
    if nargout > 1
        steps = j;
    end

end