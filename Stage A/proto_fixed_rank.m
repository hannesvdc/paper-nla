function Q = proto_fixed_rank(A, l)
% PROTO_FIXED_RANK Compute an orthonormal matrix Q whose range of rank l
% approximates the range of A. A has fast decaying singular values or is
% a small matrix.
%   Q = PROTO_FIXED_RANK(A, l) computes a matrix Q whose range approximates
%   the range of A. Q has rank l.
%
%   Implementation of algorithm 4.1.

    [~, n] = size(A);
    % Draw a random Gaussian matrix
    omega = randn(n, l);
    Y = A*omega;
    % Compute QR-factorisation with economy size
    [Q, ~] = qr(Y, 0);
end