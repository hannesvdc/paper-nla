function [Q, errors, steps] = proto_fixed_precision(A, eps, r, varargin)
% PROTO_FIXED_PRECISION Compute an orthonormal matrix Q such that 
% norm((I -QQ')A) <= eps with probability at least 1-min{m,n}10^(-r), with
% A a (m,n)-matrix. A has fast decaying singular values or is
% a small matrix.
%   Q = PROTO_FIXED_PRECISION(A, eps, r) computes a matrix Q such that 
%   norm((I -QQ')A) <= eps, with probability at least 1-min{m,n}10^(-r), 
%   with A a (m,n)-matrix.
%   
%   [Q, errors] = PROTO_FIXED_PRECISION(A, eps, r) computes a matrix Q such 
%   that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. Error matrix contains the 
%   maximum norms of the errors.
%   
%   [Q, errors, steps] = PROTO_FIXED_PRECISION(A, eps, r) computes a 
%   matrix Q such that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. Error matrix contains the 
%   maximum norms of the  errors. Steps defines the number of steps needed; 
%   thus the rank of Q.
% 
%   Q = PROTO_FIXED_PRECISION(A, eps, r, maxit) computes a matrix Q such
%   that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. It uses a maximum 
%   of maxit iterations.
%
%   [Q, errors] = PROTO_FIXED_PRECISION(A, eps, r, maxit) computes a 
%   matrix Q such that norm((I -QQ')A) <= eps, with probability at 
%   least 1-min{m,n}10^(-r), with A a (m,n)-matrix. Error matrix contains 
%   the maximum norms of the errors. It uses a maximum of maxit iterations.
%   
%   [Q, errors, steps] = PROTO_FIXED_PRECISION(A, eps, r, maxit) computes 
%   a matrix Q such that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. Error matrix contains the 
%   maximum norms of the  errors. Steps defines the number of steps needed; 
%   thus the rank of Q. It uses a maximum of maxit iterations.
% 
%   Implementation of algorithm 4.2.

    % Initialize variables
    [m, n] = size(A);
    maxit = inf;
    if nargin > 3
        maxit = varargin{1};
    end
    tol = eps/(10*sqrt(2/pi));
    
    % Draw standard Gaussian vectors
    Omega = randn(n, r);
    Y = A*Omega;      
    
    % Intialize loop
    j = 0;
    Q = zeros(m, 0);             
    errors = [];
    index = 1;
    
    % Calculate the initial errors.
    norms = sqrt(sum(Y.^2, 1))';
    
    while max(norms) > tol && j <= maxit
        j = j+1;
        
        % verbose
        if nargin > 4
            max(norms)
        end
        
        y = (eye(m) - Q*Q')*Y(:, index);
        q = y/norm(y);
        Q = [Q, q];
       
        % Output the errors if asked
        if nargout > 1
            errors = [errors, norm(A - Q*Q'*A)];
        end
        
        omega = rand(n, 1);
        Y(:, index) = (eye(m)-Q*Q')*(A*omega);
        
        for i = 1:r
            if i == index
                continue
            end
            Y(:, i) = Y(:, i) - q*(q' * Y(:,i));
        end
        
        norms = sqrt(sum(Y.^2, 1))';
        index = mod(index, r) + 1;
    end
    
    if nargout > 2
        steps = j;
    end
    
    % verbose
    if nargin > 4
        disp(['steps ', num2str(j)]);
        disp(norms);
    end
end