function Q = power_fixed_rank(A, l, q)
% POWER_FIXED_RANK Compute an orthonormal matrix Q whose range of rank l
% approximates the range of A. A has a flat singular spectrum or is a very
% large matrix.
%   Q = POWER_FIXED_RANK(A, l, q) computes a matrix Q whose range 
%   approximates the range of A. Q has rank l. It uses q powers.
%
%   Implementation of algorithm 4.3.

    [~,n] = size(A);
    
    % Draw a random Gaussian matrix
    Omega = randn(n, l);
    Y = A*Omega;
    
    % Apply powers of A
    for j = 1:q
        Y = A'*Y;
        Y = A*Y;
    end
    
    % Compute QR-factorisation
    [Q, ~] = qr(Y, 0);
end