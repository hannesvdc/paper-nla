function Q = subspace_fixed_rank(A, l, q)
% SUBSPACE_FIXED_RANK Compute an orthonormal matrix Q whose range of rank l
% approximates the range of A. A has a flat singular spectrum or is a very
% large matrix. 
%   Q = SUBSPACE_FIXED_RANK(A, l, q) computes a matrix Q whose range 
%   approximates the range of A. Q has rank l. It uses q powers.
%
%   Implementation of algorithm 4.4, a numerically stable version of 
%   algorithm 4.3. 

    [~, n] = size(A);
    
    % Draw a random Gaussian matrix
    Omega = randn(n, l);
    Y = A*Omega;
    
    % Apply after each application of A or A' a QR-factorisation.
    [Q, ~] = qr(Y, 0);
    for j = 1:q
        Y = A'*Q;
        [Q, ~] = qr(Y, 0);
        Y = A*Q;
        [Q, ~] = qr(Y, 0);
    end
end