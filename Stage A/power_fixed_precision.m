function [Q, steps] = power_fixed_precision(A, eps, r, q, varargin)
% PROTO_FIXED_PRECISION Compute an orthonormal matrix Q such that 
% norm((I -QQ')A) <= eps with probability at least 1-min{m,n}10^(-r), with
% A a (m,n)-matrix. A has a flat singular spectrum or is a very
% large matrix.
%   Q = PROTO_FIXED_PRECISION(A, eps, r, q) computes a matrix Q such that 
%   norm((I -QQ')A) <= eps, with probability at least 1-min{m,n}10^(-r), 
%   with A a (m,n)-matrix. It applies q power of A and A'.
%   
%   [Q, steps] = PROTO_FIXED_PRECISION(A, eps, r, q) computes a 
%   matrix Q such that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. 
%   Steps defines the number of steps needed; thus the rank of Q. 
%   It applies q power of A and A'.
% 
%   Q = PROTO_FIXED_PRECISION(A, eps, r, q, maxit) computes a matrix Q such
%   that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. It uses a maximum 
%   of maxit iterations. It applies q power of A and A'.
%   
%   [Q, steps] = PROTO_FIXED_PRECISION(A, eps, r, q, maxit) computes 
%   a matrix Q such that norm((I -QQ')A) <= eps, with probability at least 
%   1-min{m,n}10^(-r), with A a (m,n)-matrix. Steps defines the number
%   of steps needed; thus the rank of Q. 
%   It uses a maximum of maxit iterations. It applies q power of A and A'.
% 
%   Adaptation of algorithm 4.3 to fixed precision.

    % Initialize variables
    [m, n] = size(A);
    if nargin > 4
        maxit = varargin{1};
    else
        maxit = inf;
    end
    tol = eps/(10*sqrt(2/pi));
    
    % Draw standard Gaussian vectors
    Omega = randn(n, r);
    Y = A*Omega;
    for i = 1:q
         Y = A'*Y;
         Y = A*Y;
    end

    % Initialize loop
    j = 0;
    Q = zeros(m, 0);
    index = 1;
    
    % Calculate the initial errors.
    norms = sqrt(sum(Y.^2, 1))';
    
    while max(norms) > tol && j <= maxit
        j = j+1;
        
        % verbose
        if nargin > 4
            disp(j)
            max(norms)
        end
        y = (eye(m) - Q*Q')*Y(:, index);
        q = y/norm(y);
        Q = [Q, q];
       
        omega = rand(n, 1);

        Y(:, index) = A*omega;
        for i = 1:q
            Y(:, index) = A'*Y(:, index);
            Y(:, index) = A*Y(:, index);
        end
        Y(:, index) = (eye(m)-Q*Q')*Y(:, index);
        
        for i = 1:r
            if i == index
                continue
            end
            Y(:, i) = Y(:, i) - q*(q' * Y(:,i));
        end
        
        norms = sqrt(sum(Y.^2, 1))';
        index = mod(index, r) + 1;
    end
    
    if nargout > 1
        steps = j;
    end
    
    if nargin > 4
        disp(['Tolerance ', num2str(tol)]);
    end
end