% This function computes a reduces QR factorization of matrix A using House
% holder reflectors.
function [Q, A] = qr_householder(A)
% QR_HOUSEHOLDER Compute a QR-factorisation of a matrix A with Householder
% reflectors.
%   [Q, R] = QR_HOUSEHOLDER(A) computes a QR-factorisation of matrix A,
%   such that A = Q*R.

    [m, n] = size(A);
    reflectors = cell(n, 1);
    for k = 1:n
        x = A(k:m, k);
        e1 = zeros(m-k+1,1);
        e1(1) = 1;
        v = sign(x(1))*norm(x)*e1 + x;
        v = v/norm(v);
        reflectors{k} = v;
        
        A(k:m, k:n) = A(k:m, k:n) - 2*v*(v'*A(k:m,k:n));
        
    end
    
    % and form matrix Q
    Q = eye(m);
    for l = 1:n
        Q(:, l) = apply(Q(:, l), reflectors, m, n);
    end
    A = A(1:n, 1:n);
end

% apply the householder reflectors to a given vector
function v = apply(x, reflectors, m, n)
    for j = n:-1:1
        v = reflectors{j};
        x(j:m,1) = x(j:m, 1) - 2*v*(v'*x(j:m,1));
    end
    
    v = x;
end