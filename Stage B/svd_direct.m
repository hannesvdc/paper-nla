function [U, sigma, V] = svd_direct(A, Q)
% SVD_DIRECT Compute the singular value decomposition of the matrix 
% A, based on an approximation of the range of A, orthonormal matrix Q.
%   [U, sigma, V] = SVD_DIRECT(A, Q) computes the singular value
%   decomposition of A with an approximation of the range of A, 
%   orthonormal matrix Q, such that A =~ U*sigma*V'
%
%   Implementation of algorithm 5.1.
    B = Q'*A;
    [U, sigma, V] = svd(B);
    U = Q*U;
end