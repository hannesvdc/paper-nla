function [U, lambda] = eig_direct(A, Q)
% EIG_DIRECT Compute the eigenvalue decomposition of Hermitian matrix 
% A, based on an approximation of the range of A, orthonormal matrix Q.
%   [U, lambda] = EIG_DIRECT(A, Q) computes the eigenvalue decomposition
%   of A with approximation of the range of A by orthonormal matrix Q, such
%   that A =~ U*lambda*U'
%
%   Implementation of algorithm 5.3.

    B = Q'*A*Q;
    [V, lambda] = eig(B);
    U = Q*V;
end