\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Overview of the code}{2}
\contentsline {paragraph}{Stage A}{2}
\contentsline {section}{\numberline {3}Verification of tests}{3}
\contentsline {subsection}{\numberline {3.1}Errorbounds of stage A: Section 7.1}{3}
\contentsline {subsection}{\numberline {3.2}Slowly decaying singular values and power iteration: Section 7.2}{4}
\contentsline {subsection}{\numberline {3.3}Performance of various SVD decompositions}{6}
\contentsline {section}{\numberline {4}Extra designed tests}{7}
\contentsline {subsection}{\numberline {4.1}Timings SVD}{7}
\contentsline {subsection}{\numberline {4.2}Comparison randomized power iteration and randomized subspace iteration}{9}
\contentsline {section}{\numberline {5}Conclusion}{9}
