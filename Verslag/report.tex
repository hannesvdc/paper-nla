\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{float}
\usepackage{listings}
\usepackage{framed}
\usepackage{epstopdf}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\date{Academic year 2016-2017}

\title{Assignment 2: Numerical experiments for paper `Finding structure with randomness'}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
\maketitle	

\begin{framed}
	\centering \textbf{Abstract paper\cite{halko2011finding}}\\
	The paper \emph{Finding Structure with Randomness: Probabilistic Algorithms for Constructing Approximate Matrix Decompositions} by Halko, Martinsson and Tropp discusses known and new techniques for computing matrix decompositions based on randomized methods. They focus on explaining the randomized algorithms and compare the performance and accuracy with standard algorithms for computing the singular value decomposition of a matrix. In the final part, they also lay the basis for theoretical accuracy and convergence results \footnotemark
\end{framed}

\footnotetext{We didn't take this last part of the paper into consideration, not for our experiments, nor for our analysis.}

\tableofcontents

\section{Introduction}
 In this assignment, we will verify a couple of experiments performed in the paper and check if we get the same results for the claims they make. We obtain roughly the same results as in the paper. Then, we will also state a couple of new tests we performed to check the accuracy and running time of computing a singular value decomposition (SVD) of a random matrix. These partial results however do not show a significant speedup compared to the deterministic SVD algorithms. We also compare the accuracy of the randomized power iteration and randomized subspace iteration. Our results confirm the claim that the latter is numerically more stable than the former.	

\section{Overview of the code}
The paper clearly separates the computation of a low rank approximation $Q Q^* A$ of a matrix $A$ and applying this approximation to computing interesting factorizations of $A$. These two computations are divided into two stages, called \emph{stage A} and \emph{stage B}. In the code, we also used this separation. 

\paragraph{Stage A} Stage A contains the implementations of the following algorithms:
\begin{itemize}
	\item \texttt{proto\_fixed\_rank.m} : An implementation of Algorithm 4.1. This algorithm computes, given an $m \times n$ matrix $A$ and rank $l$, an $m \times l$ orthonormal matrix $Q$ whose range approximates the range of $A$.
	\item \texttt{proto\_fixed\_precision.m}: An implementation of Algorithm 4.2. This algorithm computes an orthonormal matrix Q such that $||(I -QQ^*)A|| \leq \epsilon$ with probability at least $1-\text{min}\{m,n\}10^{-r}$, with $A$ an $m \times n$-matrix, tolerance $\epsilon$.
	\item \texttt{power\_fixed\_rank.m}: An implementation for Algorithm 4.3. Does the same as algorithm 4.1, but with a power iteration scheme (power $q$). Is used for input matrices with a flat singular spectrum or for very large matrices. 
	\item \texttt{subspace\_fixed\_rank.m}: An implementation for Algorithm 4.4. A numerically stable alternative for the power iteration.
	\item \texttt{power\_fixed\_precision.m}: An algorithm alike algorithm 4.2 but for the power iteration method.
\end{itemize}

In stage B, the approximation $Q Q^* A$ of $A$ is used to compute low rank decompositions of $A$. More specifically, we implemented
\begin{itemize}
	\item \texttt{svd\_direct.m}: Computes the SVD based on Algorithm 5.1.
	\item \texttt{eig\_direct.m}: Computes an eigenvalue decomposition as in Algorithm 5.3.
\end{itemize}
\section{Verification of tests}
In this section, we carry out the same numerical experiments as in Section 7 of the paper. Only the experiment of Section 7.3 of the paper is not present here due to the specialized nature of Eigenfaces. The following subsections go into more detail on each experiment.

\subsection{Errorbounds of stage A: Section 7.1}
In Section 7.1 of the paper, the authors describe a test they performed on a matrix $A$ with rapidly decaying eigenvalues. They executed algorithm 4.1 to compute a low rank approximation $Q Q^* A$ for increasing values of the rank $k$. The results clearly show that the error is always bounded below by $\sigma_{k+1}$ and with a large probability above by error bound (1.9): $||A-QQ^*A|| \leq [1+9\sqrt{k+p}\sqrt{\text{min}\{m,n\}}]\sigma_{k+1}$.   The code for this experiment can be found in \texttt{exp\_section\_71.m}. A similar graph to figure 7.2 in the paper can be found in Figure \ref{fig:71}.

\begin{figure}
	\includegraphics[width=\linewidth]{fig71}
	\caption{The numerical experiment performed to check error bound (1.9) and the bound by the singular values. These results confirm the claims in the paper.}
	\label{fig:71}
\end{figure}

At first glance, the results in Figure \ref{fig:71} confirm that the numerical error in blue is bounded below by the next singular value and above by (1.9). This confirms the claim made by the authors. We used a test matrix that is a bit but not significantly different from the matrix used by the authors. For simplicity, we let the eigenvalues decay exponentially while in the paper, the singular values stall after $k = 100$. That is why the errors also seem to stall in the paper but not in our experiments. 

\subsection{Slowly decaying singular values and power iteration: Section 7.2}
In Section 7.2 of the paper, the authors test the power method on a large, sparse, noisy matrix arising in image processing. Our experiment starts from an image of a dog (Figure \ref{fig:dog}), converted to a grayscale image matrix with intensities in the range 0 to 4095. Note that we start from a $30 \times 30$ matrix instead of a $95 \times 95$ matrix. This is because of computational time and artifacts because of the processing of the intensities at the edge. In the paper they mention only that they do \emph{appropriate modifications near the edges}, not what they exactly do. So we took as pixelvector $x^{(i)} \in \mathbb{R}^{25}$ $[0.9 \dots 1.1]$ times the intensity at pixel $i$. 

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{dog}
	\caption{The image of the dog from which experiment 7.2 started.}
	\label{fig:dog}
\end{figure}

From this matrix of intensities $X$, we draw, as described in the paper, a Hermitian matrix $A$. Figure \ref{fig:72} shows the approximation errors for different trials of the iteration power scheme, up to a basis of rank 100 of matrix $A$. The exact eigenvalues are plotted in both figures. We see indeed that those eigenvalues slowly decay. In this case, the power method should be better than the prototype algorithm. 

\begin{figure}
	\centering
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.88\textwidth]{slow_decay_30_error}
		\caption{The approximation error for different trials of the power iteration scheme (up to rank l)}
		\label{fig:721}
	\end{subfigure}
	\begin{subfigure}[b]{\textwidth}
		\includegraphics[width=0.88\textwidth]{slow_decay_30_eig}
		\caption{The 100 largest estimated eigenvalues different trails of the power iteration scheme}
		\label{fig:722}
	\end{subfigure}
	\caption{The approximation error and estimated eigenvalues for different trials of the power iteration scheme}
	\label{fig:72}
\end{figure}

This is confirmed in Figure \ref{fig:721}: the prototype algorithm ($q = 0$) has the largest approximation error. Moreover, the plot confirms that the higher the power of $q$, the better the approximation is (the closer the errors are to the lower bound of the exact eigenvalues). One big difference with the results of the paper is the oscillation of the errors. This behavior is not present in the paper.

Figure \ref{fig:722} shows the estimated eigenvalues of $A$. This uses the two-stage algorithm with in stage A the power iteration scheme and in stage B the direct eigenvalue computation. The results from the paper are confirmed; the larger the power $q$, the better the estimations. Note that $q = 0$ gives very bad results. For powers $q > 0$, we see that the largest eigenvalues are indeed quite accurate, even for $q = 1$.

You can find the implementation of this test in \texttt{exp\_section\_72.m}.

\subsection{Performance of various SVD decompositions}
In Section 7.4 of the paper, the authors describe a couple of combinations they used to compute a singular value decomposition of a matrix and compared the performance. We will try to mimic these results by executing the combinations in Table \ref{tab:experiments}. Here caution is necessary. The MATLAB function \texttt{qr} is under the covers implemented in highly optimized Fortran code. As a consequence, this factorization will almost always be faster compared to the randomized algorithm. Therefore we implemented our own QR-factorization method based on Householder reflectors. This piece of code can be found in \texttt{qr\_householder.m}.
\begin{table}
	\centering
	\begin{tabular}{|l || l | l |}
		\hline
		\emph{Method} & \emph{Stage A} & \emph{Stage B} \\
		\hline
		Direct & Reduced QR-Householder & Algorithm 5.1 \\
		\hline
		Gauss & Algorithm 4.1 & Algorithm 5.1 \\
		\hline
		SVD & MATLAB SVD & Truncate \\
		\hline
	\end{tabular}
	\caption{List of the experiments performed for experiment 7.4}
	\label{tab:experiments}
\end{table}

The numerical timings are enlisted in Table \ref{tab:timings74}.
\begin{table}
	\centering
	\begin{tabular}{|c ||c | c || c | c |}
		\hline
		\emph{l} & \emph{Direct} & \emph{Gauss} & \emph{Direct} & \emph{Gauss} \\
		\hline
		10  & 0.3758  &  0.0206 & 3.2631 & 0.1037 \\
		20  & 0.3567  &  0.0309 & 3.2415 & 0.1495 \\
		40  & 0.3779  &  0.0528 & 3.2275 & 0.2487 \\
		80  & 0.4265  &  0.0595 & 3.2742 & 0.4559 \\
		160 & 0.6498  &  0.0595 & 3.9038 & 0.4137 \\
		320 & 1.5496  &  0.1566 & 5.6370 & 0.5985 \\
		640 & 4.9461  &  0.3852 & 12.2330 & 1.1477 \\
		1280 & -      &    -    & 36.4890 & 2.7557\\
		\hline
		\emph{svd} & 0.3146 & & 3.0589 & \\
		\hline
	\end{tabular}
	\caption{List of the experiments performed for experiment 7.4. The first two columns of timings are for a $1024 \times 1024$ matrix. The last two for a $2048 \times 2048$ matrix.}
	\label{tab:timings74}
\end{table}

When we compare Table \ref{tab:timings74} to Table 7.1 in the paper, we obviously see that the absolute timings are different. This is due to the 
machines we are working on. Only the relative differences in time are important. Here we can draw the same conclusion as in the paper: the SVD algorithm based on the QR-factorization is slower than the randomized algorithm. We also see that our proto svd implementation (third and fifth column) is faster than the MATLAB \texttt{svd}. This is only true because the maximal rank is 640. If we would have done the randomized SVD with rank 1024, the proto algorithm would have been much slower. Another issue are the timings in MATLAB. The \texttt{svd}-function in MATLAB is written in highly optimized Fortran code. This makes it very difficult to compare the proto algorithm with the MATLAB SVD implementation. In the paper, the authors do not have this problem as they implemented everything in Fortran.

An interesting numerical artifact in the table is that for $n = 2048$ the running time for $l=80$ is bigger than the running time for $l=160$. This occurred systematically when we ran the experiment multiple times. We have no decent explanation for this phenomenon yet.  

Another more fundamental problem is that the authors did not check accuracy in their paper. This is one of our main criticisms: we also find it important to investigate the accuracy. We have done this in one of our own designed tests (see Section \ref{sec:timings_svd}).

The code for this test is found in \texttt{exp\_section\_74.m}.

\section{Extra designed tests}
In this section we pose and perform two experiments that are not in the paper, but are useful to verify a few claims they make. The first experiment tries to determine if their new SVD algorithm is in practice better than the standard SVD algorithms, while the second test is a test on numerical stability of the randomized power iteration.

\subsection{Timings SVD}
\label{sec:timings_svd}

The experiments performed in section 7.4 of the paper compare the execution time of the randomized SVD algorithm with the standard deterministic algorithms using a fixed rank approximation. In real life however, what we really want is to choose a precision $\epsilon$ first and then decide whether we want to use a deterministic or randomized algorithm, depending on what the fastest method is. This is the goal of this section.
More specifically, we timed the execution time of the combinations in Table \ref{tab:comb5}  for various values of the precision $\epsilon$. Table \ref{tab:timings5} contains the numerical results. The matrix $A$ on which we performed the experiments has nice linearly decaying singular values which makes it a good fit for the standard proto algorithm.
\begin{table}
	\centering
	\begin{tabular}{|l || l | l |}
		\hline
		\emph{Method} & \emph{Stage A} & \emph{Stage B} \\
		\hline
		Proto & Algorithm 4.1 & Algorithm 5.1 \\
		\hline
		Power & Algorithm 4.3 & Algorithm 5.1 \\
		\hline
		SVD &  & MATLAB SVD \\
		\hline
	\end{tabular}
	\caption{List of combinations we used for the experiments in Section 5.1.}
	\label{tab:comb5}
\end{table}

\begin{table}
	\centering
	\begin{tabular}{|r||c|c|c|c|}
		\hline
		\emph{Precision} & \emph{Error proto} & \emph{Error power} & \emph{Timing proto} & \emph{Timing power} \\
		\hline
		\num{1.0} & \num{2.96e-01 } &   \num{3.08e-01}  &   \num{2.53e+00}  &   \num{3.30e+01} \\
		\num{0.1} & \num{2.93e-02}  &   \num{2.96e-02}   & \num{ 2.59e+00}    & \num{3.58e+01} \\
		\num{0.01} & \num{2.99e-03}  &   \num{2.98e-03}  &   \num{3.31e+00}    & \num{3.93e+01} \\
		\num{0.001} & \num{3.09e-04}   &  \num{3.11e-04}   &  \num{3.86e+00}    & \num{4.34e+01} \\
		\num{0.0001} & \num{3.05e-05}   &  \num{3.06e-05}  &   \num{4.31e+00}    & \num{4.79e+01} \\
		\hline
	\end{tabular}
	\caption{Timings and errors for different values of the precision of the singular value decomposition}
	\label{tab:timings5}
\end{table}
From the table we conclude that to reach a given precision for the SVD, the power iteration seems to be slower than the standard proto algorithm. This may be due to the fact that the matrix has rapidly decaying singular values such that the power iteration is not strictly necessary. We also see that the errors always stay below the given precision which validates our implementation.

When we compare the randomized algorithm with the MATLAB \texttt{svd} implementation, we again notice that MATLAB is much faster due to their highly optimized Fortran code. This makes it useless to compare the running times between standard SVD and the proto algorithm. Writing our own SVD implementation was not possible as well.

To come to terms with this problem, we compared the MATLAB \texttt{svd} algorithm and our proto algorithm on a matrix $A$ with increasing size and plotted the running times in figure \ref{fig:svdprotoruntime}. The idea is that the proto algorithm is better when we see that it has a smaller proportionality constant than MATLAB. This means that, if we choose the size large enough, our implementation will beat MATLAB. The experiments were performed for $\epsilon=1$.

In the figure, we see that the MATLAB SVD and the proto SVD have approximately the same proportionality constant for $\epsilon=1$. From this plot alone, it is not obvious to draw a strong conclusion, and more experiments are needed for different sizes and values of the precision. The evidence is however not in favor of the proto algorithm.
\begin{figure}
	\includegraphics[width=\linewidth]{svdtimings}
	\caption{Timings of the Matlab SVD and Proto SVD. It can be seen that both methods have about the same proportionality constant for $\epsilon=1$.}
	\label{fig:svdprotoruntime}
\end{figure}

\texttt{svdconstants.m} and \texttt{precisiontest.m} contain the code for this test.

\subsection{Comparison randomized power iteration and randomized subspace iteration}
The authors of the paper developed a randomized power iteration scheme for large matrices with a flat spectrum. They claim that this algorithm (algorithm 4.3) is not numerically stable, because of rounding errors in floating-point arithmetic. More precisely, they claim that all information associated with singular values smaller than $\mu^{1/(2q+1)}||A||$, with $\mu$ the machine-precision and $q$ the power, is lost. 


To verify this claim, we designed an experiment where we construct a matrix $A \in \mathbb{R}^{200 \times 200}$ with a norm of 1 and the rest of the singular values exponentially decreasing from one to zero. The higher $q$, the more singular values are smaller than the bound of $\mu^{1/(2q+1)||A||}$ (for $q = 0$, this is $\num{2.22e-16}$ (two singular values are lost), for $q = 1, \num{6.0555e-6}$ (we lose 134 values), for $q = 2, \num{7.4010e-04}$ (there are 160 singular values smaller) and for $q = 3, \num{0.0058}$ (there are 171 singular values smaller than the bound)). The larger $q$, the better the randomized subspace iteration should perform relative to randomized power iteration. We took the mean of the errors for 5 experiments for different powers of $q$. Figure \ref{fig:pp} shows the results. 

We see that, as $q$ increases, the power iteration tends to stall earlier due to roundoff errors, while the subspace iteration keeps on converging linearly until machine precision. The position where the power iteration starts to stall is exactly at the position of the singular value that we lose first. For $q=0$ this does not happen (no power of $A$ are applied), for $q=1$ it occurs at position 70, for $q=2$ at around 40 and for $q=3$ around 30. These results confirm the claims of the paper.

\begin{figure}
	\centering
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{pp_500_0}
		\caption{Comparison of the mean error of the randomized power iteration and randomized subspace iteration for power 0 (proto algorithm)}
		\label{fig:pp0}
	\end{subfigure}
	\begin{subfigure}[b]{\textwidth}
		\centering		
		\includegraphics[width=0.7\textwidth]{pp_500_1}
		\caption{Comparison of the mean error of the randomized power iteration and randomized subspace iteration for power 1}
		\label{fig:pp1}
	\end{subfigure}
\end{figure}
\begin{figure}
	\ContinuedFloat 
	\begin{subfigure}[b]{\textwidth}
		\centering		
		\includegraphics[width=0.7\textwidth]{pp_500_2}
		\caption{Comparison of the mean error of the randomized power iteration and randomized subspace iteration for power 2}
		\label{fig:pp2}
	\end{subfigure}
	\begin{subfigure}[b]{\textwidth}
		\centering		
		\includegraphics[width=0.7\textwidth]{pp_500_3}
		\caption{Comparison of the mean error of the randomized power iteration and randomized subspace iteration for power 3}
		\label{fig:pp3}
	\end{subfigure}
	\caption{Comparison of the mean error of the randomized power iteration and randomized subspace iteration for different powers}
	\label{fig:pp}
\end{figure}

You can find the implementation of this test in \texttt{powerprecision.m}.

\section{Conclusion}
In this paper we implemented the randomized proto and power algorithm to produce a low rank approximation of a matrix. Then we used this approximation to compute a SVD, eigenvalue decomposition, etc., of the matrix. We first verified the upper and lower bound on the approximation error as claimed in the paper. Secondly we compared the accuracy of the standard proto algorithm and the power iteration on a matrix with slowly decaying singular values. Third, we found the same relative performance of classical direct methods, compared to our randomized algorithms.
After verifying these claims by the authors, we also performed experiments in which we compare the running time of the proto SVD algorithm with the standard SVD. Here we do not have strong conclusions due to the MATLAB/Fortran implementation. Finally, we also checked the numerical stability of the randomized power iteration in comparison with the randomized subspace iteration. We were able to confirm the claims of the authors.
\bibliographystyle{plain}
\bibliography{biblio}
\end{document}